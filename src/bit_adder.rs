pub struct BitAdder {
    pub sum: i8,
    pub carry: i8,
}

pub trait BitAdderExecute {
    fn execute(&mut self, a: i8, b: i8, c: i8);
}

pub trait BitAdderGetValues {
    fn get_sum(&self) -> i8;
    fn get_carry(&self) -> i8;
}

impl BitAdderExecute for BitAdder {
    fn execute(&mut self, a: i8, b: i8, c: i8) {
        self.sum = (a ^ b) ^ c;
        self.carry = (a & b) | ((a ^ b) & c);
    }
}

impl BitAdderGetValues for BitAdder {
    fn get_sum(&self) -> i8 {
        self.sum
    }

    fn get_carry(&self) -> i8 {
        self.carry
    }
}