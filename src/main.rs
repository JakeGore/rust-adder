mod bit_adder;
mod helpers;

use bit_adder::*;
use helpers::{get_numeric_input};

fn main() {
    let mut adder = BitAdder { sum: 0, carry: 0 };
    let mut input_numbers: [i8; 2] = [0; 2];

    input_numbers = input_numbers.map(|_| get_numeric_input());

    let mut total_sum: i8 = 0;
    let mut current_carry: i8 = 0;

    for n in 0..8 {
        let bit1 = (input_numbers[0] >> n) & 0x1;
        let bit2 = (input_numbers[1] >> n) & 0x1;

        adder.execute(bit1, bit2, current_carry);

        total_sum |= adder.get_sum() << n;
        current_carry = adder.get_carry();
    }

    println!("{} + {} = {}", input_numbers[0], input_numbers[1], total_sum);
}