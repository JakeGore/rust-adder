use std::io;

pub fn get_numeric_input() -> i8 {
    print!("Enter a number: ");

    match io::Write::flush(&mut io::stdout()) {
        Ok(_) => { /* Do nothing */ },
        _ => panic!("Failed to flush stdout buffer")
    }

    let mut input_text = String::new();

    match io::stdin().read_line(&mut input_text) {
        Ok(_) => { /* Do nothing */ },
        _ => panic!("Failed to read input")
    }

    match input_text.trim().parse::<i8>() {
        Ok(number) => number,
        _ => panic!("{} is an invalid number", input_text.trim())
    }
}